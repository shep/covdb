# Import nextstrain-formatted metadata to database
from initdb import Sample
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, declarative_base
import pandas as pd
import numpy as np
from datetime import date
import re
from dbconnect import get_db_engine

DATE_FULL="(\d+)-(\d+)-(\d+)"
DATE_MONTH="(\d+)-(\d+)"


# CLI parsing
import argparse
parser=argparse.ArgumentParser(description="Import nextstrain-formatted metadata to database")
parser.add_argument("metafile", metavar="META")
parser.add_argument("--origin", metavar="ORIGIN", default="gisaid")
args=parser.parse_args()


# Standardized date string to object
def dateparse(dstr):
    day_missing=True
    month_missing=True
    ddate=None

    if re.match(DATE_FULL, dstr):
        ddate=date.fromisoformat(dstr)
        day_missing=False
        month_missing=False
    elif re.match(DATE_MONTH, dstr):
        ddate=date.fromisoformat(dstr + "-01")
        month_missing=False
    elif re.match("\d{4}", dstr):
        ddate=date.fromisoformat(dstr + "-01-01")


    return {"date":ddate,"month_missing":month_missing ,"day_missing":day_missing}



# Database interface initialization
e=get_db_engine()
s=Session(e)



# Load metadata file
metafile=args.metafile
sam=pd.read_csv(metafile, sep="\t")

# Format metadata file
sam["age"]=pd.to_numeric(sam["age"], errors="coerce", downcast="integer")
sam["sex"]=[ l if l in ["Male", "Female"] else None for l in sam["sex"] ]
sam[sam == "?"]=np.nan
sam=sam.where(pd.notnull(sam), None)

# Shorten fields to fit
MAX_LENGTH=250
sam["originating_lab"]=[ l[:MAX_LENGTH - 1] for l in sam["originating_lab"]]
sam["submitting_lab"]=[ l[:MAX_LENGTH - 1] for l in sam["submitting_lab"]]

for i,l in sam.iterrows():
    # Skip if exists
    if s.query(s.query(Sample).filter(Sample.id == str(l["strain"])).exists()).scalar():
        continue

    # Get date details
    ddate=dateparse(l["date"])

    snew=Sample(id=str(l["strain"]), gisaid_epi_isl=l["gisaid_epi_isl"],
                date=ddate["date"], date_missing_day=ddate["day_missing"],
                date_missing_month=ddate["month_missing"], origin=args.origin,
                region=l["region"], country=l["country"], division=l["division"], location=l["location"],
                region_exposure=l["region_exposure"], country_exposure=l["country_exposure"],
                division_exposure=l["division_exposure"], age=None if np.isnan(l["age"]) else int(l["age"]), sex=l["sex"],
                originating_lab=l["originating_lab"], date_submitted=dateparse(l["date_submitted"])["date"])
    if "reinfected" in sam.columns:
        s.reinfected=l["reinfected"]
        s.severe_or_critical=l["severe_or_critical"]
        s.vaccinated=l["vaccinated"]
        s.returnee_from_abroad=l["returnee_from_abroad"]
    s.add(snew)
    s.commit()
