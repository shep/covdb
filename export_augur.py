# coding: utf-8
from sqlalchemy import create_engine, Date, ForeignKey, Table, Column, Integer, String, Text, Boolean, ForeignKey, select
from sqlalchemy.orm import declarative_base, relationship, Session
from sqlalchemy.sql import exists
from initdb import Sample, RNASequence
import pandas as pd
from dbconnect import get_db_engine

# CLI parsing
import argparse
parser=argparse.ArgumentParser(description="Export augur-formatted metadata and sequence files")
parser.add_argument("meta_out", metavar="META")
parser.add_argument("seq_out", metavar="FASTA")
args=parser.parse_args()

# Create metadata entry for sample
def addSample(samp):
    sam=pd.DataFrame([{
        "strain":samp.id,
        "date":samp.date_output(),
        "virus":"ncov",
        "region":samp.region}],
        columns=["strain", "date", "virus", "region"])
    return sam

# Database interface initialization
e=get_db_engine()
s=Session(e)

# Initialize standard metadata table
MANDATORY_FIELDS=["strain", "date", "virus", "region"]
cs=MANDATORY_FIELDS
sam=pd.DataFrame(columns=cs)

# Iterate samples
ss=s.execute(select(Sample))
seqs={}
for so in ss:
    so=so[0]
    if so.sequences:
        # Import metadata to table
        sam=sam.append(addSample(so))

        # Add sequence
        seqs[so.id]=so.sequences[0]
sam.to_csv(args.meta_out, sep="\t", index=None)

# Output Fasta
with open(args.seq_out, "w") as o:
    for sname in sam["strain"]:
        o.write(">S" + sname + "\n")
        o.write(seqs[sname].sequence + "\n")

