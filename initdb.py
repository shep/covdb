# Initialize database structure
from sqlalchemy import create_engine, Date, ForeignKey, Table, Column, Integer, String, Text, Boolean, ForeignKey
from sqlalchemy.orm import declarative_base, relationship, deferred
from sqlalchemy_utils import database_exists, create_database
from dbconnect import get_db_engine


# Database interface initialization
e=get_db_engine()
if not database_exists(e.url):
    create_database(e.url)
Base = declarative_base()


# Table declaration
class Sample(Base):
    __tablename__="sample"

    id=Column(String(250), primary_key=True)
    date=Column(Date(), nullable=True)
    date_missing_day=Column(Boolean(), nullable=True)
    date_missing_month=Column(Boolean(), nullable=True)
    origin=Column(String(20), nullable=True)
    gisaid_epi_isl=Column(String(20), nullable=True)

    # Geographical parameters
    #Region from: Africa”, “Asia”, “Europe”, “North America”, “Oceania”, “South America”
    region=Column(String(15), nullable=True)
    country=Column(String(150), nullable=True)
    division=Column(String(150), nullable=True)
    location=Column(String(150), nullable=True)

    #If there is no travel history then set this to be the same value as region.
    region_exposure=Column(String(150), nullable=True)
    country_exposure=Column(String(150), nullable=True)
    division_exposure=Column(String(150), nullable=True)

    # Host parameters
    age=Column(Integer(), nullable=True)
    sex=Column(String(20), nullable=True)
    reinfected=Column(Boolean(), nullable=True)
    severe_or_critical=Column(Boolean(), nullable=True)
    vaccinated=Column(Boolean(), nullable=True)
    returnee_from_abroad=Column(Boolean(), nullable=True)

    # Submitter parameters
    originating_lab=Column(String(250), nullable=True)
    submitting_lab=Column(String(250), nullable=True)
    date_submitted=Column(Date(), nullable=True)

    # Sequence relation
    sequences=relationship("RNASequence", back_populates="sample")


    # Output date in an augur-compatible format
    def date_output(self):
        o=[str(self.date.year)]
        if not self.date_missing_month:
            o.append(str(self.date.month).rjust(2, "0"))
        if not self.date_missing_day:
            o.append(str(self.date.day).rjust(2, "0"))

        return "-".join(o)

    def __repr__(self):
        return f"Sample {self.id} from {self.origin}"


class RNASequence(Base):
    __tablename__="sequence"

    id=Column(String(250), primary_key=True)
    origin=Column(String(30))
    sequence=deferred(Column(Text()))
    sample_id=Column(String(250), ForeignKey("sample.id"))
    sample=relationship("Sample", back_populates="sequences")

    def __repr__(self):
        return f"Sequence {self.id} from {self.origin}"



Base.metadata.create_all(e)
