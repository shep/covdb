# Establish relation between sequences and samples
from sqlalchemy import create_engine, Date, ForeignKey, Table, Column, Integer, String, Text, Boolean, ForeignKey, select
from sqlalchemy.orm import declarative_base, relationship, Session
from sqlalchemy.sql import exists
from initdb import Sample, RNASequence
import re
from dbconnect import get_db_engine


# Database interface initialization
e=get_db_engine()
Base = declarative_base()
s=Session(e)

#TODO: Generalize
# Iterate sequences
ss=s.execute(select(RNASequence))
for seq in ss:
    seq=seq[0]

    if not seq.sample:
        # Get sample ID
        sid=re.search("(\d{7})", seq.id)
        if sid:
            sid=sid.group(1)
        else:
            sid=seq.id
        sid=str(sid)

        # Find sample
        samp=s.execute(select(Sample).where(Sample.id == sid)).fetchone()
        if samp:
            samp=samp[0]
            seq.sample=samp
            s.flush()
            print("Assigned sequence", seq.id, "to sample", samp.id)

s.commit()
