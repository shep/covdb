# coding: utf-8
import sqlalchemy
from sqlalchemy import create_engine
import json



def get_db_engine(configfile="config.json"):
    CONNECTION_STRING="mysql+pymysql://{username}:{password}@{host}:{port}/{db_name}"

    # Load configurations from JSON
    conf=json.load(open(configfile))

    e=create_engine(CONNECTION_STRING.format(**conf))

    return e
