from initdb import RNASequence
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, declarative_base
import pysam
from dbconnect import get_db_engine


# CLI parsing
import argparse
parser=argparse.ArgumentParser(description="Import Fasta sequences to database")
parser.add_argument("sequences", metavar="FASTA")
parser.add_argument("--origin", metavar="ORIGIN", default="gisaid")
args=parser.parse_args()


# Database interface initialization
e=get_db_engine()
s=Session(e)


fa=pysam.FastaFile(args.sequences)

ff=[]
for nn in fa.references:
    snew=RNASequence(id=nn, origin=args.origin, sequence=fa.fetch(nn))
    ff.append(snew)

# Additions
s.add_all(ff)
s.commit()
